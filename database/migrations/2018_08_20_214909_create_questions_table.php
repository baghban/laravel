<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable(false);
            $table->float('point')->nullable(false);
            $table->unsignedInteger('part_id')->nullable(false);
            $table->unsignedInteger('lesson_id')->nullable(true);
            $table->unsignedInteger('question_difficulty_id')->nullable(false);
            $table->unsignedInteger('bloom_taxonomy_id')->nullable(true);
            $table->unsignedInteger('question_type_id')->nullable(false);
            $table->timestamp('created_at')->useCurrent()->nullable(false);
            $table->timestamp('updated_at')->useCurrent()->nullable(false);
            $table->foreign('part_id')->references('id')->on('parts');
            $table->foreign('lesson_id')->references('id')->on('lessons');
            $table->foreign('question_difficulty_id')->references('id')->on('question_difficulties');
            $table->foreign('bloom_taxonomy_id')->references('id')->on('bloom_taxonomies');
            $table->foreign('question_type_id')->references('id')->on('question_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
